package main
	
import (
  "fmt"
  "io/ioutil"
  "net/http"
  "os"
  "os/exec"
  "strings"
  "regexp"
  gt "github.com/bas24/googletranslatefree"
  "gopkg.in/yaml.v3"
)

type Talimat struct {
  Isim string
  Isim2 string 
  Surum string
  Surum2 string
  Devir string
  Tanim string
  Url string
  Paketci string
  Dgerek string
  Gerek string
  Sha256 string
  Sha512 string
  Kaynak string
  Derle string
  Dtip string
  Ekconf string
  Pakur string
  Tip string
}

type Solus struct {
     Name     string `yaml:"name"`
     Version  string `yaml:"version"`
     Release  string `yaml:"release"`
     Sources  []interface{} `yaml:"source"`
     Homepage string `yaml:"homepage"`
     Licence  string `yaml:"licence"`
     Component string `yaml:"component"`
     Summary  string `yaml:"summary"`
     Desc string `yaml:"description"`
     Bdeps []string `yaml:"builddeps"`
     Rdeps string `yaml:"rundeps"`
     Build string `yaml:"build"`
     Setup string `yaml:"setup"`
     Install string `yaml:"install"`
}

func adopt_vars(str *string) {
	vars := map[string]string {
		"\"$srcdir\"": "$SRC",
		"${FILESDIR}": "$SRC",
		"$srcdir": "$SRC",
		"${srcdir}": "$SRC",
		"\"$pkgdir\"": "$PKG",
		"\"${pkgdir}\"": "$PKG",
		"$pkgdir": "$PKG",
		"\"$DESTDIR\"": "$PKG",
		"${pkgdir}": "$PKG",
		"$pkgbase": "$isim2",
		"$pkgname": "$isim",
		"$name": "$isim",
		"${name": "${isim",
		"${pkgname}": "$isim",
		"$pkgver": "$surum",
		"${pkgver}": "$surum",
		"$version": "$surum",
		"${version}": "$surum",
		"${version": "$surum",
		"$_pkgver": "$surum2",
		"$pkgrel": "$devir",
		"${homepage}": "$url",
		"v$surum.tar.gz": "v$surum.tar.gz::$isim-$surum.tar.gz",
	}
	for src,tgt := range vars {
		*str = strings.Replace(*str, src, tgt,-1)
	}
}	

func adopt_parens(str *string) {
	vars := map[string]string {
		"'": "",
		"\"": "",
		"(": "",
		")": "",
		"{,.sig}": "",
		"{,.asc}": "",
		"'SKIP'": "",
		"-devel": "",
		"${sourcepkg}>=${version}_${revision}": "",
		"#commit=$_commit": "",
	}
    for src,tgt := range vars {
		*str = strings.Replace(*str, src, tgt,-1)
	}
}

func Tprint(t Talimat) {
	content := ""
	// meta
	t.Paketci = "milisarge"
    
    content += fmt.Sprintln("[paket]")
    // google çeviri
    if t.Tanim != "" {
      t.Tanim, _ = gt.Translate(t.Tanim, "en", "tr")
    }
    adopt_parens(&t.Tanim)
    content += fmt.Sprintln("tanim   =",t.Tanim)
    content += fmt.Sprintln("paketci =",t.Paketci)
    content += fmt.Sprintln("grup    =","kütüphane")
    adopt_parens(&t.Url)
    content += fmt.Sprintln("url     =",t.Url)
    content += fmt.Sprintln()
   
    // deps
    adopt_parens(&t.Dgerek)
    adopt_parens(&t.Dgerek)
    content += fmt.Sprintln("[gerek]")
    t.Dgerek = strings.Join(strings.Fields(t.Dgerek), " ")
    content += fmt.Sprintln("derleme =",t.Dgerek)
    content += fmt.Sprintln("calisma =",t.Gerek)
    content += fmt.Sprintln()
    
    // source
    content += fmt.Sprintln("[kaynak]")
    adopt_vars(&t.Kaynak)
    adopt_parens(&t.Kaynak)
    for i,src := range strings.Split(t.Kaynak, "\n") {
	  if strings.Contains(src, "git+") {
		src = strings.Split(src, "?")[0]
		content += fmt.Sprintln("git","=",strings.Replace(src,"git+","",-1))
	  } else {
	    content += fmt.Sprintln(i+1,"=",src)
	  }
	}
    content += fmt.Sprintln()
    
    //sha256
    if t.Sha256 != "" {
		content += fmt.Sprintln("[sha256]")
		adopt_parens(&t.Sha256)
		for i,sha256 := range strings.Split(t.Sha256, "\n") {
		  content += fmt.Sprintln(i+1,"=",sha256)
		}
		content += fmt.Sprintln()
	}	
    
     //sha512
    if t.Sha512 != "" {
		content += fmt.Sprintln("[sha512]")
		adopt_parens(&t.Sha512)
		for i,sha512 := range strings.Split(t.Sha512, "\n") {
		  content += fmt.Sprintln(i+1,"=",sha512)
		}
		content += fmt.Sprintln()
	}
    // replace variables
    adopt_vars(&t.Derle)
    adopt_vars(&t.Pakur)
    
    content += fmt.Sprintln("[derle]")
    if t.Derle != "" {
		content += fmt.Sprintln(t.Derle)
	}
    if t.Ekconf != "" {
		content += fmt.Sprintln("ekconf =",t.Ekconf)
	}
    if t.Dtip != "" {
		content += fmt.Sprintln("tip = ",t.Dtip)
	}
	content += fmt.Sprintln()	
    
    content += fmt.Sprintln("[pakur]")
    if t.Tip != "" {
		content += fmt.Sprintln("tip = ",t.Tip)
	}
    content += fmt.Sprintln(t.Pakur)
    // talimat içerik yaz.
    fmt.Println("-------------- MİLİS -------------------------")
    fmt.Println("isim    =",t.Isim)
    if t.Isim2 != "" {
	  fmt.Println("isim2   =",t.Isim2)
	}
    fmt.Println("surum   =",t.Surum)
    if t.Surum2 != "" {
      fmt.Println("surum2   =",t.Surum2)
	}
    fmt.Println("devir   =",t.Devir)
    fmt.Println(content)
    fmt.Println("---------------------------------------")
    //talimat dosya yaz.
    tdir := fmt.Sprintf("/tmp/%s#%s-%s", t.Isim, t.Surum, t.Devir)
    cmd := "mkdir -p %s"
    exec.Command("sh", "-c",fmt.Sprintf(cmd,tdir)).Output()
    err := os.WriteFile(tdir + "/talimat", []byte(content), 0664)
    if err != nil {
	  fmt.Printf("talimat oluşturulamadı: %s\n", err)
	  os.Exit(1)
	} else {
	  fmt.Println("Talimat dizini :", tdir)
	}
}

func arch_conv(restxt string) Talimat {
	var t Talimat
	
	sha256mode := false
	sha512mode := false
	bldmode := false
	pakmode := false
	depmode := false
	srcmode := false
	bracket_open := false
	paren_open := false
	fmt.Println("-------------- ARCH -------------------------")
	for _,line := range(strings.Split(restxt,"\n")) {
	  fmt.Println(line)
	  if strings.HasPrefix(strings.TrimLeft(line, " "),"#") {continue}
	  if strings.Contains(line,"provides=") {continue}
	  if strings.Contains(line,"conflicts=") {continue}
	  if strings.Contains(line,"sha256sums=('SKIP')") {continue}
	  if strings.Contains(line,"z.sig") {continue}
	  if strings.Contains(line,"pkgbase=") {
	    t.Isim2 = strings.Replace(strings.Split(line,"pkgbase=")[1],"'","",-1)
	    continue
	  }
	  if strings.Contains(line,"pkgname=") {
	    t.Isim = strings.Replace(strings.Split(line,"pkgname=")[1],"'","",-1)
	    t.Isim = strings.TrimSpace(t.Isim)
	    adopt_parens(&t.Isim)
	    t.Isim = strings.Fields(t.Isim)[0]
	    continue
	  }
	  if strings.Contains(line,"_pkgver=") {
	    t.Surum2 = strings.Split(line,"_pkgver=")[1]
	    continue
	  }
	  if strings.Contains(line,"pkgver=") {
	    t.Surum = strings.Split(line,"pkgver=")[1]
	    continue
	  }
	  if strings.Contains(line,"pkgrel=") {
	    t.Devir = strings.Split(line,"pkgrel=")[1]
	    continue
	  }
	  if strings.Contains(line,"pkgdesc=") {
	    t.Tanim = strings.Replace(strings.Split(line,"pkgdesc=")[1],"'","",-1)
	    continue
	  }
	  if strings.Contains(line,"url=") {
	    t.Url = strings.Replace(strings.Split(line,"url=")[1],"'","",-1)
	    continue
	  }
	  if strings.Contains(line,"prepare()") {
	    bldmode = true
	    continue
	  }
	  if strings.Contains(line,"build()") {
	    bldmode = true
	    continue
	  }
	  if strings.Contains(line,"package_") {
	    pakmode = true
	    continue
	  }
	  if strings.Contains(line,"package()") {
	    pakmode = true
	    continue
	  }
	  
	  if strings.Contains(line,"source=(") {
		srcmode = true
		line = strings.Replace(line,"source=(","",-1)
		t.Kaynak += line
		// tek satırlık atama
		if strings.Contains(line,")"){
			srcmode = false
		} else {
			continue
		}
	  }

	  if strings.Contains(line,"sha256sums=(") {
		sha256mode = true
		line = strings.Replace(line,"sha256sums=(","",-1)
		t.Sha256 += line
		// tek satırlık atama
		if strings.Contains(line,")"){
			sha256mode = false
		} else {
			continue
		}
	  }
	  if sha256mode {
		t.Sha256 += "\n" + strings.TrimSpace(line)
	    if strings.Contains(line,")") {
		  sha256mode = false
		  continue
		} 
	  }
	  
	  if strings.Contains(line,"sha512sums=(") {
		sha512mode = true
		line = strings.Replace(line,"sha512sums=(","",-1)
		t.Sha512 += line
		// tek satırlık atama
		if strings.Contains(line,")"){
			sha512mode = false
		} else {
			continue
		}
	  }
	  if sha512mode {
		t.Sha512 += "\n" + strings.TrimSpace(line)
	    if strings.Contains(line,")") {
		  sha512mode = false
		  continue
		} 
	  }
	  
	  for _,depi := range []string{"makedepends=","optdepends=","depends="} {
		  if strings.Contains(line,depi) {
			depmode = true
			line = strings.Replace(line,depi,"",-1)
			// tek satırlık atama
			if strings.Contains(line,")"){	
				t.Dgerek += line +" "
				depmode = false
			} else {
				continue
			}
		  }
	  }
	  
	  if strings.Contains(line,"{") {
	    bracket_open = true
	  }
	  if bracket_open && strings.Contains(line,"}") {
	    bracket_open = false
	  }
	  if strings.Contains(line,"(") {
	    paren_open = true
	  }
	  if paren_open && strings.Contains(line,")") {
	    paren_open = false
	  }
	  if bldmode {
	    if ! bracket_open && strings.Contains(line,"}") && ! strings.Contains(line,"{") {
		  bldmode = false
		  continue
		}
		if line == "" {continue}
	    t.Derle += "betik = " + strings.TrimLeft(line, " ") +"\n"
	  }
	  if pakmode {
	    if ! bracket_open && strings.Contains(line,"}") && ! strings.Contains(line,"{") {
		  pakmode = false
		  continue
		} 
		if line == "" {continue}
	    t.Pakur += "betik = " + strings.TrimLeft(line, " ") +"\n"
	  }
	  if depmode {
	    if ! paren_open && strings.Contains(line,")") && ! strings.Contains(line,"(") {
		  depmode = false
		  continue
		} 
		line = strings.Split(line, ":")[0]	
	    t.Dgerek += line +" "
	  }
	  if srcmode {
		t.Kaynak += "\n" + strings.TrimSpace(line)
	    if strings.Contains(line,")") {
		  srcmode = false
		  continue
		} 
	  }  
	} // end parsing
	return t
}

func void_conv(template string) Talimat {
	var t Talimat
	depmode := false
	dermode := false
	pakmode := false
	ekconfmode := false
	
	pakur_re := regexp.MustCompile(`post_install|do_install`)
	derle_re := regexp.MustCompile(`post_extract()|pre_configure()|pre_build()`)
	
	fmt.Println("-------------- VOID -------------------------")
	for _,line := range(strings.Split(template,"\n")) {
	  fmt.Println(line)
	  
	  if strings.Contains(line,"pkgname=") {
	    t.Isim = strings.Replace(strings.Split(line,"pkgname=")[1],"'","",-1)
	    t.Isim = strings.TrimSpace(t.Isim)
	    continue
	  }
	  if strings.Contains(line,"short_desc=") {
	    t.Tanim = strings.Replace(strings.Split(line,"short_desc=")[1],"'","",-1)
	    continue
	  }
	  if strings.Contains(line,"version=") {
	    t.Surum = strings.Replace(strings.Split(line,"version=")[1],"'","",-1)
	    continue
	  }
	  if strings.Contains(line,"revision=") {
	    t.Devir = strings.Replace(strings.Split(line,"revision=")[1],"'","",-1)
	    continue
	  }
	  if strings.Contains(line,"build_style=") {
		if strings.Contains(line,"gnu-configure") { 
			t.Dtip  = "gnu"
			t.Tip  = "gnu"
		}
		if strings.Contains(line,"cmake") { 
			t.Dtip  = "cmake"
			t.Tip  = "cmake"
			t.Dgerek += "cmake "
		}
		if strings.Contains(line,"meson") { 
			t.Dtip  = "meson"
			t.Tip  = "meson"
			t.Dgerek += "meson "
		}
		if strings.Contains(line,"cargo") { 
			t.Derle  += "betik = rust_setup.sh -y && export PATH=$PATH:$HOME/.cargo/bin && cargo build --release\n"
		}
	    continue
	  }
	  if strings.Contains(line,"configure_args=") {
	    t.Ekconf = strings.Replace(strings.Split(line,"configure_args=")[1],"'","",-1)
	    // tek satır ise " bitiyorsa ekconf bitti
	    if strings.HasSuffix(line,"\"") {
		  ekconfmode = false
		  t.Ekconf = strings.Replace(t.Ekconf,"\"","",-1)
		  continue
	    }
	    ekconfmode = true
	    continue
	  }
	  if strings.Contains(line,"homepage=") {
	    t.Url = strings.Replace(strings.Split(line,"homepage=")[1],"'","",-1)
	    continue
	  }
	  if strings.Contains(line,"checksum=") {
	    t.Sha256 = strings.Replace(strings.Split(line,"checksum=")[1],"'","",-1)
	    continue
	  }
	  if strings.Contains(line,"distfiles=") {
	    t.Kaynak = strings.Replace(strings.Split(line,"distfiles=")[1],"'","",-1)
	    continue
	  }
	  for _,depi := range []string{"hostmakedepends=","makedepends=","optdepends=","depends="} {
		  if strings.Contains(line,depi) {
			depmode = true
			line = strings.Replace(line,depi,"",-1)
			t.Dgerek += line +" "
			// tek satırlık atama
			if strings.Contains(line,"\""){	
				depmode = false
			} else {
				continue
			}
		  }
	  }
	  // pakur
	  if pakur_re.MatchString(line) {
		pakmode = true
	    continue
	  }
	  
	  if derle_re.MatchString(line) {
		dermode = true
	    continue
	  }
	 
	  if depmode {
	    t.Dgerek += line +" "
	    if strings.Contains(line,"\""){	depmode = false }
	  }
	
	  if dermode {
		if line == "" {continue}
	    if ! strings.Contains(line,"{") && strings.Contains(line,"}"){	
			dermode = false
			continue 
		}
		t.Derle += "betik =" + line +"\n"
	  }
	  if pakmode {
		if line == "" {continue}
	    if ! strings.Contains(line,"{") && strings.Contains(line,"}"){	
			pakmode = false
			continue 
		}
		// vbin, vinstall düzenleme
		if strings.Contains(line, "vbin ") {
			line = strings.Replace(line, "vbin ", "cp -rf  ",-1) + " $PKG/usr/bin/"
		}
		if strings.Contains(line, "vlicense") {continue}
	    t.Pakur += "betik =" + line +"\n"
	  }
	  if ekconfmode {
		t.Ekconf += line
		continue
	  }
	}
	return t
}

func crux_conv(template string) Talimat {
	var t Talimat
	dermode := false
	pakmode := false
	srcmode := false
	
	fmt.Println("-------------- CRUX -------------------------")
	for _,line := range(strings.Split(template,"\n")) {
	  fmt.Println(line)
	  
	  if strings.Contains(line,"name=") {
	    t.Isim = strings.Replace(strings.Split(line,"name=")[1],"'","",-1)
	    t.Isim = strings.TrimSpace(t.Isim)
	    continue
	  }
	  if strings.Contains(line,"# Description:") {
	    t.Tanim = strings.Replace(strings.Split(line,"# Description:")[1],"'","",-1)
	    continue
	  }
	  if strings.Contains(line,"version=") {
	    t.Surum = strings.Replace(strings.Split(line,"version=")[1],"'","",-1)
	    continue
	  }
	  if strings.Contains(line,"release=") {
	    t.Devir = strings.Replace(strings.Split(line,"release=")[1],"'","",-1)
	    continue
	  }
	 
	  if strings.Contains(line,"source=(") {
		srcmode = true
		line = strings.Replace(line,"source=(","",-1)
		t.Kaynak += line
		// tek satırlık atama
		if strings.Contains(line,")"){
			srcmode = false
		} else {
			continue
		}
	  }
	  if strings.Contains(line,"# URL:") {
	    t.Url = strings.TrimSpace(strings.Replace(strings.Split(line,"# URL:")[1],"'","",-1))
	    continue
	  }
	  if strings.Contains(line,"# Depends on:") {
	    t.Dgerek = strings.Replace(strings.Split(line,"# Depends on:")[1],"'","",-1)
	    continue
	  }
	  // pakur
	  if strings.Contains(line,"build()") {
	    dermode = true
	    continue
	  }
	  if dermode {
		if line == "" {continue}
	    if ! strings.Contains(line,"{") && strings.Contains(line,"}"){	
			dermode = false
			continue 
		}
	    t.Derle += "betik =" + line +"\n"
	  }
	  if pakmode {
		if line == "" {continue}
	    if ! strings.Contains(line,"{") && strings.Contains(line,"}"){	
			pakmode = false
			continue 
		}
	    t.Pakur += "betik =" + line +"\n"
	  }
	  if srcmode {
		t.Kaynak += "\n" + strings.TrimSpace(line)
	    if strings.Contains(line,")") {
		  srcmode = false
		  continue
		} 
	  } 
	  
	}
	return t
}

func solus_conv(template string) Talimat {
	
	var t Talimat
	var s Solus
	yaml.Unmarshal([]byte(template), &s)
	fmt.Println("-------------- SOLUS -------------------------")
	fmt.Println(template)
	t.Isim  = s.Name
	t.Surum = s.Version
	t.Devir = "1" //s.Release
	t.Tanim = s.Desc
	for nr, source := range(s.Sources) {
	  for k,v := range source.(map[string]interface{}) {
        t.Kaynak += k
        // isim-surum kayıt için
        fmt.Println(nr,k)
        if strings.Contains(t.Kaynak,"archive/refs/tags") && nr == 0 {
	      t.Kaynak += "::$isim-$surum" + ".tar." +strings.Split(t.Kaynak, "tar.")[1]
	    }
	    t.Kaynak += " "
        t.Sha256 += v.(string) + ""
      }
      
	}
	t.Url = s.Homepage
	autogen := false
	reconfigure := false
	configure := false
	cmake := false
	cmake_ninja := false
	meson := false
	cmake_opts := ""
	conf_opts := ""
	meson_opts := ""
	// pre-build
	s.Setup = strings.Replace(s.Setup, "$pkgfiles","$SRC",-1)
	s.Setup = strings.Replace(s.Setup, "$installdir","$PKG",-1)
	s.Setup = strings.Replace(s.Setup, "%patch","patch",-1)
	s.Setup = strings.Replace(s.Setup, "$sources","$SOURCES_DIR",-1)
	//fmt.Println("?",s.Setup,"?")
	for _, line := range strings.Split(s.Setup,"\n") {
	  line = strings.TrimLeft(line," ")
	  if line != "" {
	    // yorum tespit
	    if strings.HasPrefix(line, "#") {
		  t.Pakur += fmt.Sprintf("; %s \n",line)
		  continue
		}
		// --option
		if strings.HasPrefix(line, "--") {
		  if autogen || configure || reconfigure {
		    conf_opts += line
		  }
		  if cmake {
		    cmake_opts += line
		  }
		  continue
	    }
		// autogen
		if strings.Contains(line, "%autogen") {
		  autogen = true
		  conf_opts += strings.Replace(strings.Split(line, "%autogen")[1],"\\", "", -1)
		  continue
	    }
		// configure
	    if strings.Contains(line, "%configure") {
		  configure = true
		  conf_opts = strings.Replace(strings.Split(line, "%configure")[1],"\\", "", -1)
		  continue
	    }
	    if line == "%reconfigure" {
		  reconfigure = true
		  continue
	    }
	    if strings.Contains(line, "%meson_configure") {
		  meson = true
		  meson_opts += strings.Replace(strings.Split(line, "%meson_configure")[1],"\\", "", -1)
		  continue
	    }
	    if strings.Contains(line, "%cmake_ninja") {
		  cmake_ninja = true
		  cmake_opts = strings.Replace(strings.Split(line, "%cmake_ninja")[1],"\\", "", -1)
		  continue
	    }
	    if strings.Contains(line, "%cmake") {
		  cmake = true
		  cmake_opts = strings.Split(line, "%cmake")[1]
		  continue
	    }
        t.Derle += fmt.Sprintf("betik = %s \n",line)
      }
	}
	// build
	s.Build = strings.Replace(s.Build, "$pkgfiles","$SRC",-1)
	s.Build = strings.Replace(s.Build, "$installdir","$PKG",-1)
	s.Build = strings.Replace(s.Build, "%patch","patch",-1)
	s.Build = strings.Replace(s.Build, "$sources","$SOURCES_DIR",-1)
	for _, line := range strings.Split(s.Build,"\n") {
	  if line != "" {
		line = strings.TrimLeft(line," ")
	    // yorum tespit
	    if strings.HasPrefix(line, "#") {
		  t.Pakur += fmt.Sprintf("; %s \n",line)
		  continue
		}
		if line == "%ninja_build" && meson {
		  if meson_opts != "" {
		    t.Derle += fmt.Sprintf("ekconf = %s \n",meson_opts)
	      }
		  t.Derle += "tip   = meson\n"
		  continue 
		}
		if strings.Contains(line, "%make") && (reconfigure || configure || autogen) {
		  if autogen {
		    t.Derle += "betik  = ./autogen.sh\n"
		  }
		  if conf_opts != "" {
		    t.Derle += fmt.Sprintf("ekconf = %s \n",conf_opts)
	      }
		  t.Derle += "tip   = gnu\n"
		  continue 
		}
		if cmake || cmake_ninja {
		  cmake_opts = strings.TrimLeft(cmake_opts," ")
		  if cmake_opts != "" {
		    t.Derle += fmt.Sprintf("ekconf = %s \n",cmake_opts)
	      }
		  t.Derle += fmt.Sprintf("tip    = cmake \n")
		  t.Dgerek += " cmake"
		  continue
		}
		if strings.Contains(line, "%make") {
		  line = strings.Replace(line,"%make", "make ${MAKEJOBS}", -1)
		}
		t.Derle += fmt.Sprintf("betik = %s \n",line)
      }
	}
	
	// pakur
	s.Install = strings.Replace(s.Install, "$pkgfiles","$SRC",-1)
	s.Install = strings.Replace(s.Install, "$installdir","$PKG",-1)
	s.Install = strings.Replace(s.Install, "$sources","$SOURCES_DIR",-1)
	for _, line := range strings.Split(s.Install,"\n") {
	  if line != "" {
	    line = strings.TrimLeft(line," ")
	    // yorum tespit
	    if strings.HasPrefix(line, "#") {
		  t.Pakur += fmt.Sprintf("; %s \n",line)
		  continue
		}
		if line == "%ninja_install" && meson {
		  t.Pakur += "tip   = meson\n"
		  continue 
		}
		//fmt.Println(fmt.Sprintf(":%s:",line))
	    if strings.Contains(line, "%make_install") {
		  if cmake {
		    t.Pakur += fmt.Sprintf("tip   = %s\n","cmake")
		  } else {
		    t.Pakur += fmt.Sprintf("tip   = %s\n","gnu")
	      }
		  continue
		}
		if strings.Contains(line, "%ninja_install") {
		  if cmake_ninja {
		    t.Pakur += fmt.Sprintf("tip   = %s\n","cmake")
	      }
		  continue
		} 
		t.Pakur += fmt.Sprintf("betik = %s \n",line)
      }
	}
	// pkgconfig ayrıştırma
	r := regexp.MustCompile(`pkgconfig\((.*?)\)`)
    matches := r.FindAllStringSubmatch(strings.Join(s.Bdeps,"+"), -1)
    for _, v := range matches {
      if v[1] != "" {
	    t.Dgerek += " " + v[1]
	  }
    }   
	
	// trim left
	t.Dgerek = strings.TrimLeft(t.Dgerek," ")
	// brevity for desc
	t.Tanim = strings.Split(t.Tanim,".")[0]+"."
	return t
}

func usage() {
  fmt.Println("kullanım: talimatci -depo-harf paket_ismi")
  os.Exit(1)
}

func main() {
	var requestURL string
	if len(os.Args) < 3 {
	  usage()
	}
	depo := os.Args[1]
	paket := os.Args[2]
	repos := map[string]string {
	  "-aur" : "paket",
	  "-a" : "paket",
	  "-m" : "{core/extra}/paket",
	  "-ka" : "apps/master/paket",
	  "-k" : "{core,main}/master/paket ",
	  "-v" : "https://raw.githubusercontent.com/void-linux/void-packages/master/srcpkgs/%s/template",
	  "-c" : "https://crux.nu/ports/crux-3.7/%s/Pkgfile",
	}
	urls := map[string]string {
	  "-aur" : "https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=%s",
	  "-a" : "https://gitlab.archlinux.org/archlinux/packaging/packages/%s/-/raw/main/PKGBUILD",
	  "-m" : "https://gitlab.manjaro.org/packages/%s/-/raw/master/PKGBUILD",
	  "-ka" : "https://raw.githubusercontent.com/KaOSx/apps/master/%s/PKGBUILD",
	  "-k" : "https://raw.githubusercontent.com/KaOSx/%s/PKGBUILD",
	  "-v" : "https://raw.githubusercontent.com/void-linux/void-packages/master/srcpkgs/%s/template",
	  "-c" : "https://crux.nu/ports/crux-3.7/%s/Pkgfile",
	  "-s" : "https://raw.githubusercontent.com/getsolus/packages/refs/heads/main/packages/%s/package.yml",
	}
	fns := map[string]func(string) Talimat {
	  "-aur" : arch_conv,
	  "-a" : arch_conv,
	  "-m" : arch_conv,
	  "-k" : arch_conv,
	  "-ka" : arch_conv,
	  "-v" : void_conv,
	  "-c" : crux_conv,
	  "-s" : solus_conv,
	  // solus h/htop şeklinde çağrılacak
	}
	fn := fns[depo]
	
	if strings.Contains(paket,"https://") {
	  requestURL = paket
	} else {
	  requestURL = fmt.Sprintf(urls[depo],paket)
	}
	fmt.Println("url:",requestURL)
	req, err := http.NewRequest(http.MethodGet, requestURL, nil)
	if err != nil {
		fmt.Printf("client: could not create request: %s\n", err)
		os.Exit(1)
	}

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		fmt.Printf("client: error making http request: %s\n", err)
		os.Exit(1)
	}

	restxt, err := ioutil.ReadAll(res.Body)
	if err != nil {
		fmt.Printf("client: could not read response body: %s\n", err)
		os.Exit(1)
	}
	
	if res.StatusCode != 200 || strings.Contains(string(restxt),"DOCTYPE html") {
	  fmt.Println("BULUNAMADI:",paket,depo)
	  fmt.Println(repos[depo])
	  os.Exit(1)
	} 
	
	// ilgili çeviriciye gönder.
    ta := fn(string(restxt))
    Tprint(ta)
}
