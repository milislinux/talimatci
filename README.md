
## Talimatci

Diğer dağıtımların paket yapma betiklerini Milis talimatlarına çeviren konsol uygulaması.

#### Kurulum
talimatci.tar.xz arşiv dosyasını indirerek dışarı çıkarın.
<p>
talimatci uygulamasını çalışabilir yol dizini altına kopyalayın.

#### Kullanım
```
# void
talimatci -v paket_ismi

# pacman kullananlar
talimatci -a PKGBUILD_linki

# arch
talimatci -a paket_ismi

# aur
talimatci -aur PKGBUILD_linki

# manjaro
talimatci -m {core/extra}/paket_ismi

# kaosx apps
talimatci -ka paket_ismi

# kaosx main/core
talimatci -k {core,main}/master/paket_ismi

# crux
talimatci -c port_isim/paket_ismi

```

- İlgili paketin bulunup çevrilmesinden sonra /tmp altına talimat dizini oluşturulacaktır.
